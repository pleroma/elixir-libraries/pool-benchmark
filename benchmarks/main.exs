defmodule AnoPool.Benchmark.Helpers do
  def handle_mint_response(conn, ref, state \\ %{}) do
    receive do
      message ->
        case Mint.HTTP.stream(conn, message) do
          :unknown ->
            IO.inspect(message)
            handle_mint_response(conn, ref, state)

          {:ok, conn, responses} ->
            state =
              Enum.reduce(responses, state, fn
                {:status, ^ref, status_code}, state ->
                  Map.put(state, :status_code, status_code)

                {:headers, ^ref, headers}, state ->
                  Map.put(state, :headers, headers)

                {:data, ^ref, chunk}, state ->
                  if data = state[:data] do
                    data = data <> chunk
                    %{state | data: data}
                  else
                    Map.put(state, :data, chunk)
                  end

                {:done, ^ref}, state ->
                  Map.put(state, :fin, true)

                message, _ ->
                  raise "Unhandled mint message, probably wrong ref. #{inspect(message)}"
              end)

            unless state[:fin] do
              handle_mint_response(conn, ref, state)
            else
              {state.status_code, state.headers, state.data, conn}
            end
        end
    end
  end
end

{opts, argv, _} =
  OptionParser.parse(System.argv(),
    strict: [input: [:string, :keep], job: [:string, :keep], list: :boolean]
  )

host = List.first(argv) || "127.0.0.1"

inputs =
  Enum.flat_map(
    %{
      "get:10kb" => "/10240",
      "get:1mb" => "/1048576",
      "get:20mb:chunked:1mb" => "/20971520/1048576",
      "get:20mb" => "/20971520"
    },
    fn {key, path} ->
      [
        {"http:tcp:" <> key, {path, :tcp, :http, host, 4545}},
        {"http2:tcp:" <> key, {path, :tcp, :http2, host, 4546}},
        {"http:tls:" <> key, {path, :tls, :http, host, 4547}},
        {"http2:tls:" <> key, {path, :tls, :http2, host, 4548}}
      ]
    end
  )
  |> Enum.into(%{})

# inputs = Map.put(inputs, "httpbin", {"/bytes/1048576", :tls, :http2, "httpbin.org", 443})
# inputs = Map.put(inputs, "httpbin HTTP1", {"/bytes/1048576", :tls, :http, "httpbin.org", 443})

jobs = %{
  "gun" =>
    {fn {path, transport, proto, host, port} ->
       {:ok, conn} = :gun.open(host, port, %{protocols: [proto], transport: transport})

       {:ok, ^proto} = :gun.await_up(conn)
       stream = :gun.get(conn, path)
       {:response, :nofin, status, headers} = :gun.await(conn, stream)
       {:ok, body} = :gun.await_body(conn, stream)
       {status, headers, body, conn}
     end,
     before_scenario: fn {path, transport, proto, host, port} ->
       {path, transport, proto, to_charlist(host), port}
     end,
     after_each: fn {_, _, _, conn} ->
       :gun.shutdown(conn)
     end},
  "gun:no_open_time" =>
    {fn {path, conn} ->
       stream = :gun.get(conn, path)
       {:response, :nofin, status, headers} = :gun.await(conn, stream)
       {:ok, body} = :gun.await_body(conn, stream)
       {status, headers, body, conn}
     end,
     before_scenario: fn {path, transport, proto, host, port} ->
       {path, transport, proto, to_charlist(host), port}
     end,
     before_each: fn {path, transport, proto, host, port} ->
       {:ok, conn} = :gun.open(host, port, %{protocols: [proto], transport: transport})

       {:ok, ^proto} = :gun.await_up(conn)
       {path, conn}
     end,
     after_each: fn {_, _, _, conn} ->
       :gun.shutdown(conn)
     end},
  "mint" =>
    {fn {path, scheme, proto, host, port, transport_opts} ->
       {:ok, conn} =
         Mint.HTTP.connect(scheme, host, port,
           protocols: [proto],
           transport_opts: transport_opts
         )

       {:ok, conn, request_ref} = Mint.HTTP.request(conn, "GET", path, [], nil)
       AnoPool.Benchmark.Helpers.handle_mint_response(conn, request_ref)
     end,
     before_scenario: fn {path, transport, proto, host, port} ->
       {scheme, transport_opts} =
         case transport do
           :tcp -> {:http, []}
           :tls -> {:https, [verify: :verify_none]}
         end

       proto =
         case proto do
           :http -> :http1
           proto -> proto
         end

       {path, scheme, proto, host, port, transport_opts}
     end,
     after_each: fn {_, _, _, conn} ->
       Mint.HTTP.close(conn)
     end},
  "mint:no_open_time" =>
    {fn {path, conn} ->
       {:ok, conn, request_ref} = Mint.HTTP.request(conn, "GET", path, [], nil)
       AnoPool.Benchmark.Helpers.handle_mint_response(conn, request_ref)
     end,
     before_scenario: fn {path, transport, proto, host, port} ->
       {scheme, transport_opts} =
         case transport do
           :tcp -> {:http, []}
           :tls -> {:https, [verify: :verify_none]}
         end

       proto =
         case proto do
           :http -> :http1
           proto -> proto
         end

       {path, scheme, transport_opts, proto, host, port}
     end,
     before_each: fn {path, scheme, transport_opts, proto, host, port} ->
       {:ok, conn} =
         Mint.HTTP.connect(scheme, host, port,
           protocols: [proto],
           transport_opts: transport_opts
         )

       {path, conn}
     end,
     after_each: fn {_, _, _, conn} ->
       Mint.HTTP.close(conn)
     end}
}

if opts[:list] do
  IO.puts("Inputs:")
  Enum.each(inputs, fn {key, _} -> IO.puts("- #{key}") end)
  IO.puts("\nJobs:")
  Enum.each(jobs, fn {key, _} -> IO.puts("- #{key}") end)
else
  inputs =
    case Keyword.get_values(opts, :input) do
      [] -> inputs
      list -> Map.take(inputs, list)
    end

  jobs =
    case Keyword.get_values(opts, :job) do
      [] -> jobs
      list -> Map.take(jobs, list)
    end

  Benchee.run(jobs,
    inputs: inputs,
    after_each: fn {status, headers, body, _} ->
      200 = status

      {"content-type", "application/octet-stream"} =
        Enum.find(headers, fn
          {"content-type", _} -> true
          _ -> false
        end)

      size = bit_size(body)
      <<0::size(size)>> = body
      :timer.sleep(10)
    end
  )
end
